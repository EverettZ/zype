export let BibleTableOfContents: Bible[]  = [
    {
        name: "Old Testament",
        books: [
            // Pentateuch
            {
                name: "Genesis",
                numChapters: 50
            },
            {
                name: "Exodus",
                numChapters: 40
            },
            {
                name: "Leviticus",
                numChapters: 27
            },
            {
                name: "Numbers",
                numChapters: 36
            },
            {
                name: "Deuteronomy",
                numChapters: 34
            },

            // Historical Books
            {
                name: "Joshua",
                numChapters: 24
            },
            {
                name: "Judges",
                numChapters: 21
            },
            {
                name: "Ruth",
                numChapters: 4
            },
            {
                name: "1 Samuel",
                numChapters: 31
            },
            {
                name: "2 Samuel",
                numChapters: 24
            },
            {
                name: "1 Kings",
                numChapters: 22
            },
            {
                name: "2 Kings",
                numChapters: 25
            },
            {
                name: "1 Chronicles",
                numChapters: 29
            },
            {
                name: "2 Chronicles",
                numChapters: 36
            },
            {
                name: "Ezra",
                numChapters: 10
            },
            {
                name: "Nehemiah",
                numChapters: 13
            },
            {
                name: "Esther",
                numChapters: 10
            },

            // Books if Wisdon (or "Poetry")
            {
                name: "Job",
                numChapters: 42
            },
            {
                name: "Psalms",
                numChapters: 150
            },
            {
                name: "Proverbs",
                numChapters: 31
            },
            {
                name: "Ecclesiastes",
                numChapters: 12
            },
            {
                name: "Song of Solomon",
                numChapters: 8
            },

            // Major Prophets
            {
                name: "Isaiah",
                numChapters: 66
            },
            {
                name: "Jeremiah",
                numChapters: 52
            },
            {
                name: "Lamentations",
                numChapters: 5
            },
            {
                name: "Ezekiel",
                numChapters: 48
            },
            {
                name: "Daniel",
                numChapters: 12
            },

            // Minor Prophets
            
            {
                name: "Hosea",
                numChapters: 14
            },
            {
                name: "Joel",
                numChapters: 3
            },
            {
                name: "Amos",
                numChapters: 9
            },
            {
                name: "Obadiah",
                numChapters: 1
            },
            {
                name: "Jonah",
                numChapters: 4
            },
            {
                name: "Micah",
                numChapters: 7
            },
            {
                name: "Nahum",
                numChapters: 3
            },
            {
                name: "Habakkuk",
                numChapters: 3
            },
            {
                name: "Zephaniah",
                numChapters: 3
            },
            {
                name: "Haggai",
                numChapters: 2
            },
            {
                name: "Zachariah",
                numChapters: 14
            },
            {
                name: "Malachi",
                numChapters: 4
            }
        ]
    },
    {
        name: "New Testament",
        books: [
            // Gospels
            {
                name: "Matthew",
                numChapters: 28
            },
            {
                name: "Mark",
                numChapters: 16
            },
            {
                name: "Luke",
                numChapters: 24
            },
            {
                name: "John",
                numChapters: 21
            },

            // History
            {
                name: "Acts",
                numChapters: 28
            },

            // Pauline Epistles
            {
                name: "Romans",
                numChapters: 16
            },
            {
                name: "1 Corinthians",
                numChapters: 16
            },
            {
                name: "2 Corinthians",
                numChapters: 13
            },
            {
                name: "Galatians",
                numChapters: 6
            },
            {
                name: "Ephesians",
                numChapters: 6
            },
            {
                name: "Phillippians",
                numChapters: 4
            },
            {
                name: "Colossians",
                numChapters: 4
            },
            {
                name: "1 Thessalonians",
                numChapters: 5
            },
            {
                name: "2 Thessalonians",
                numChapters: 3
            },
            {
                name: "1 Timothy",
                numChapters: 6
            },
            {
                name: "2 Timothy",
                numChapters: 4
            },
            {
                name: "Titus",
                numChapters: 3
            },
            {
                name: "Philemon",
                numChapters: 1
            },

            //General Epistles
            
            {
                name: "Hebrews",
                numChapters: 13
            },
            {
                name: "James",
                numChapters: 5
            },
            {
                name: "1 Peter",
                numChapters: 5
            },
            {
                name: "2 Peter",
                numChapters: 3
            },
            {
                name: "1 John",
                numChapters: 5
            },
            {
                name: "2 John",
                numChapters: 1
            },
            {
                name: "3 John",
                numChapters: 1
            },
            {
                name: "Jude",
                numChapters: 1
            },

            // Apocalyptic Writings
            {
                name: "Revelation",
                numChapters: 22
            }
        ]
    }
]

class Bible {
    name: string;
    books: BibleBook[];
}

class BibleBook {
    name: string;
    numChapters: number;
}