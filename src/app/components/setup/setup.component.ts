import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { GameOptionsType, GameOptions, GameOptionEnum } from '../../models/game-options';
import { Game } from '../../models/game';
import { GameService } from '../../services/game.service';
import { Observable } from 'rxjs/rx';
import { BibleTableOfContents } from '../../../assets/data/bible-table-contents';

@Component({
  selector: 'zype-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.scss']
})
export class SetupComponent implements OnInit {
  _GameOptionEnum = GameOptionEnum;
  @Input('gameTypes') gameTypes: GameOptionsType[] = [];
  @Input('selectableTimes') selectableTimes: number[] = [];
  @Input('defaultRepetitions') defaultRepetitions: number = null;

  @Output('createGame')
  createGame: EventEmitter<Observable<Game>> = new EventEmitter<Observable<Game>>();

  @ViewChild('customInput') customInput;

  selectedGameType: GameOptionsType = { name: '', value: -1 };
  repetitions = 4;
  timeLimit = 0;
  customText = '';
  testaments = BibleTableOfContents;
  Arr = Array;

  constructor(private _game: GameService) {}

  ngOnInit() {
    if (this.gameTypes.length === 0) {
      this.gameTypes = GameOptions;
    }
    this.selectedGameType = this.gameTypes[0];

    if (this.defaultRepetitions === null) {
      this.defaultRepetitions = 4;
    }
    this.repetitions = this.defaultRepetitions;

    if (this.selectableTimes.length === 0) {
      this.selectableTimes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    }
  }

  selectGameType(type: GameOptionsType) {
    this.selectedGameType = type;

    if (this.selectedGameType.value === GameOptionEnum.Custom && this.customInput) {
      setTimeout(() => {
        this.customInput.nativeElement.focus();
      }, 5);
    }
  }

  buildGameAndCreate() {
    this._game.setupGameOptions(this.repetitions, this.selectedGameType, this.timeLimit, this.customText);
    this.createGame.emit();
  }

  selectChapter(testamentName: string, bookName: string, bookChapter: number) {
    console.log(testamentName)
    console.log(bookName)
    console.log(bookChapter)
  }

}
