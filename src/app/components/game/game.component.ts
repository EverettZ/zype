import { Component, OnInit, Input } from '@angular/core';
import { Game } from '../../models/game';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/Operators';
import { GameService } from '../../services/game.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FormControl, NgControl } from '@angular/forms';

@Component({
  selector: 'zype-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  $game: Observable<Game>;
  
  constructor(
    private _game: GameService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.$game = this._game
      .createGame();
  }

  updateGameInput(ev) {  }

}
