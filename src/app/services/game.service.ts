import { Injectable } from "@angular/core";
import { Game } from "../models/game";
import { GameOptionsType, SetupOptions } from "../models/game-options";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs/rx";
import { map } from "rxjs/operators";
import { EsvApiResponse } from "../models/esv-api-response";

@Injectable()
export class GameService {
  game: Game;
  setupOptions: SetupOptions;

  // URL_BIBLE: string = "https://raw.githubusercontent.com/thiagobodruk/bible/master/json/en_bbe.json";
  URL_BIBLE: string = "https://api.esv.org/v3/passage/text/?q=Jn11.35";

  constructor(private http: HttpClient) {}

  createGame(
    repetitions: number = this.setupOptions.repetitions,
    selectedGameType: GameOptionsType = this.setupOptions.selectedGameType,
    timeLimit: number = this.setupOptions.timeLimit,
    customText: string = this.setupOptions.customText
  ) {
    let verse = "";

    if (customText.length > 0) {
      verse = customText;

      this.game = new Game(
        selectedGameType.name,
        repetitions,
        verse,
        timeLimit
      );

      return Observable.of(this.game);
    } else {
      return this.http.get(this.URL_BIBLE).pipe(
        map((res: EsvApiResponse) => {
          verse = res.passages
            .map(element => {
              element = element.substr(element.indexOf(']') + 1).replace(" (ESV)", "").trim();
              return element;
            })
            .join(" - ");

          console.log(verse);

          this.game = new Game(
            selectedGameType.name,
            repetitions,
            verse,
            timeLimit
          );

          return this.game;
        })
      );
    }
  }
  startGame() {
    this.game.startGame();
  }

  getRandomBibleVerse() {}

  getRandomDictionaryTerm() {}

  setupGameOptions(
    repetitions: number,
    selectedGameType: GameOptionsType,
    timeLimit: number,
    customText: string
  ) {
    this.setupOptions = {
      repetitions: repetitions,
      selectedGameType: selectedGameType,
      timeLimit: timeLimit,
      customText: customText
    };
  }
}
