import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from "@angular/common/http";

import { Observable } from "rxjs/Observable";

@Injectable()
export class TokenInterceptorService implements HttpInterceptor {
  ESVBibleURL = "a177227e522a07ec409758d4c99c1c68e502d8b6";
  constructor() {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    request = request.clone({
      setHeaders: {
        Authorization: `Token ${this.ESVBibleURL}`
      }
    });
    return next.handle(request);
  }
}
