export enum GameOptionEnum {
    BibleVerses,
    WordOfTheDay,
    Custom
};

export type GameOptionsType = {
    name: string,
    value: number
}

export let GameOptions: GameOptionsType[] = [
  {
    name: "Bible Verses",
    value: GameOptionEnum.BibleVerses
  },
  {
    name: "Word of the Day",
    value: GameOptionEnum.WordOfTheDay
  },
  {
    name: "Custom",
    value: GameOptionEnum.Custom
  }
];

export type SetupOptions = {
  repetitions: number,
  selectedGameType: GameOptionsType,
  timeLimit: number,
  customText: string
}
