import { Phrase } from './phrase';
import { Guid } from './guid';
import { ControlContainer, AbstractControl, NgControl, ControlValueAccessor, FormControl } from '@angular/forms';
import { Subject } from 'rxjs/Subject';
import { Word } from './word';

export class Game {
    id = '';
    name = '';
    reps = 1;

    isTimed = false;
    allowedDuration = 0;

    startTime: Date;
    endTime: Date;
    millisecondsElapsed = 0;
    secondsElapsed = 0;
    minutesElapsed = 0;

    value: Phrase[] = [];
    currentWord: Word;
    totalWords = 0;
    currentPhrase: Phrase;

    inputChange: Subject<string> = new Subject();
    inputVal = '';

    lettersCorrect = 0;
    wordsCorrect = 0;
    wpm = 0;

    keystrokes = 0;

    constructor(name: string, reps: number = 1, phraseStr: string, allowedDuration: number = 0) {

        this.id = Guid();
 
        for (let i = 0; i < reps; i++) {
            this.value.push(new Phrase(phraseStr, i));
        }

        this.isTimed = allowedDuration > 0;
        this.allowedDuration = allowedDuration;

        this.totalWords = phraseStr.length * reps;

        this.name = name;

        this.currentPhrase = this.value[0];
        this.currentWord = this.currentPhrase.value[0];
    }

    startGame() {

        this.currentPhrase = this.value[0];
        this.startTime = new Date();

        this.inputChange.subscribe(ok => {
            const lastLetter = ok.substr(-1, 1) || '';
            // debugger;
            if (lastLetter === ' ') {
                this.nextWord();
                this.inputVal = '';
            }
            this.currentWord.updateCurrValue(ok);
        });

        if (this.isTimed) {
            console.log('Its timed!');
            setTimeout(() => {
                this.endGame();
            }, this.allowedDuration);
        }
        this.startTicker();
    }

    endGame() {
        this.inputVal = '';
        this.endTime = new Date();
        this.wpm = this.calcAveragedWPM();
    }

    calcTechnicalWPM() {
        this.wordsCorrect = 0;
        for (const phrase of this.value) {
            this.wordsCorrect += phrase.calcWordsCorrect();
        }
        return this.wpm;
    }

    calcAveragedWPM() {
        this.wordsCorrect = 0;
        for (const phrase of this.value) {
            this.wordsCorrect += phrase.calcWordsCorrect();
        }
        return this.wordsCorrect / this.getElapsedTimeInMinutes();
    }

    getElapsedTimeInSeconds() {
        const currTime = new Date();
        this.millisecondsElapsed = currTime.getMilliseconds() - this.startTime.getMilliseconds();
        this.secondsElapsed /= 1000;

        this.secondsElapsed = this.secondsElapsed;
        return this.secondsElapsed;
    }

    getElapsedTimeInMinutes() {
        this.minutesElapsed = this.getElapsedTimeInSeconds() / 60;
        return this.minutesElapsed;
    }

    calcLettersCorrect() {

    }

    calcWordsCorrect() {

    }

    startTicker() {

    }

    updateInput(val) {
        console.log('UPDATE');
        this.inputChange.next(val);
    }

    nextPhrase() {
        if (this.currentPhrase.index < this.value.length - 1) {
            this.currentPhrase = this.value[this.currentPhrase.index + 1];
            this.currentWord = this.currentPhrase.value[0];
        } else {
            this.currentPhrase = null;
            this.currentWord = null;
        }
        this.inputVal = '';
    }

    nextWord() {
        if (this.currentWord.index < this.currentPhrase.value.length - 1) {
            this.currentWord = this.currentPhrase.value[this.currentWord.index + 1];
        } else if (this.currentPhrase.index < this.value.length - 1) {
            this.nextPhrase();
        } else {
            this.endGame();
        }
        this.inputVal = '';

    }

}
