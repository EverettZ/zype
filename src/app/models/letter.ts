import { Guid } from './guid';

export class Letter {
    id = '';

    value = '';
    inputValue = '';

    isCorrect = false;
    isTouched = false;

    constructor (letterString: string) {
        this.id = Guid();
        this.value = letterString;
    }

    updateCurrValue(val: string) {
        this.inputValue = val;
        this.isTouched = true;
        this.isCorrect = this.checkCorrect();
    }

    checkCorrect() {
        return this.inputValue === this.value;
    }

}
