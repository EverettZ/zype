import { Letter } from "./letter";
import { Guid } from "./guid";

export class Word {
    name: string = "";
    id: string = "";
    index: number = 0;
    
    value: Letter[] = [];

    lettersCorrect: number = 0;

    isCorrect: boolean = false;
    isTouched: boolean = false;

    constructor(wordString: string, index: number) {
        this.id = Guid();
        this.value = this.createValueArray(wordString);
        this.index = index;
    }

    createValueArray(wordString: string) {
        let result = [];
        for(let l of wordString) {
            result.push(new Letter(l));
        }
        return result;
    }

    calcWordCorrect() {
        this.lettersCorrect = this.value.filter((letter: Letter) => letter.checkCorrect()).length;
        this.isCorrect = this.lettersCorrect === this.value.length;
        return this.isCorrect;
    }

    updateCurrValue(val: string) {
        this.value.forEach((letter: Letter, index: number) => {
            letter.updateCurrValue(val.charAt(index));
        })
    }

}
