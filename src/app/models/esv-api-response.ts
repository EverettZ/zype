export class EsvApiResponse {
    canonical: string = "";
    parsed: number[] = [];
    passage_meta: PassageMeta[] = [];
    passages: string[] = [];
    query: string = "";
}

class PassageMeta {
    canonical: string = "";
    chapter_start: number[] = [];
    chapter_end: number[] = [];
    next_chapter: number[] = [];
    prev_chapter: number[] = [];
    next_verse: number = 0;
    prev_verse: number = 0;
}