import { Word } from "./word";
import { Guid } from "./guid";

export class Phrase {
    name: string = "";
    id: string = "";
    index: number = 0;
    value: Word[] = [];

    wordsCorrect: number = 0;

    isCorrect: boolean = false;
    isTouched: boolean = false;

    constructor(phraseString: string, index: number) {
        this.id = Guid();
        this.value = this.createValueArray(phraseString);
        this.index = index;
    }

    createValueArray(phraseString: string) {
        return phraseString
            .split(" ")
            .map((element, index) => new Word(element, index));
    }


    calcWordsCorrect() {
        this.wordsCorrect = this.value.filter(word => word.calcWordCorrect()).length;    
        return this.wordsCorrect;
    }


}
