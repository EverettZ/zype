import { Component } from "@angular/core";
import { Observable } from "rxjs/rx";
import { Game } from "./models/game";
import { Router } from "@angular/router";
import { map } from "rxjs/operators";

@Component({
  selector: "zype-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "ZypeType";

  constructor(private routing: Router) {}

  createGame() {
    this.routing.navigate(["/play"], { skipLocationChange: true });
  }
}
